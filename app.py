import hydra
from omegaconf import DictConfig, OmegaConf
from dataclasses import dataclass

from omegaconf import MISSING, OmegaConf

import hydra
from hydra.core.config_store import ConfigStore
import logging

# @dataclass
# class PreInput:
    
# log = logging.getLogger(__name__)


# @dataclass 
# class Base_method:
#     pass

# @dataclass
# class NER(Base_method):
#     pass

# @dataclass
# class pyate(Base_method):
#     pass 

# @dataclass 
# class phrase_chunking(Base_method):
#     pass 

@dataclass
class Rake():
    min_character: int = MISSING # minimum characters in keywords
    max_words: int = MISSING # max words in keywords
    repeat: int = MISSING  # minimum times the keywords repeat in the document



@dataclass 
class Customer:
    corpus_file: str = MISSING 
    source_sentence_col_name: str = MISSING 
    keep_other_columns: bool = MISSING 
    glossary_file: str = MISSING 



@dataclass
class Method:
    sentence_tokenizer: str = MISSING 
    glossary_generation_method: str = MISSING 

@dataclass
class PostProcess:
    glossary_file: str = MISSING # the output of the intermediate result
    sheet_comment: str = MISSING


# @dataclass
# class Sentiment:
#     glossary_file: str = MISSING # the output of the intermediate result
#     column: str = MISSING # target column that stores glossaries
#     sentiment_file: str = MISSING 


@dataclass
class Config:
    customer: Customer = MISSING
    method: Method = MISSING
    rake: Rake = MISSING 
    postprocess: PostProcess = MISSING


cs= ConfigStore.instance() 
cs.store(name='customer', node=Customer)
cs.store(name='method', node=Method)
cs.store(name='rake', node=Rake)
cs.store(name='post', node=PostProcess)

@hydra.main(config_path='configs', config_name="config")
def app_run(cfg: Config) -> None:
    # cfg.postprocess.glossary_file = cfg.customer.glossary_file

    print(OmegaConf.to_yaml(cfg))
    
    # log.info("Info level message")

    # print('Input file:')
    # print(cfg.customer.corpus_file)
    
    # if not cfg.preprocessing.initiate:
    #     print('Preprocessing is skipping')


if __name__ == "__main__":
    app_run()